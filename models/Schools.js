"use strict";

module.exports = function(sequelize, DataTypes) {
  var School = sequelize.define("Schools", {
    id: {
    	type: DataTypes.INTEGER,
    	primaryKey: true
    },
    name: DataTypes.STRING
  });

  sequelize.models.Schools = School;

  return School;
}