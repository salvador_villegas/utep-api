"use strict";

module.exports = function(sequelize, DataTypes) {
  var Nanny = sequelize.define("Nannies", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    username: DataTypes.STRING,
    name: DataTypes.STRING,
    sfdc_id: DataTypes.STRING,
    SchoolId: DataTypes.INTEGER/*{
       type: DataTypes.INTEGER,

       references: {
         // This is a reference to another model
         model: sequelize.models.Schools,

         // This is the column name of the referenced model
         key: 'id',

         // This declares when to check the foreign key constraint. PostgreSQL only.
    //     deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE
       }
     }*/
  });

//        Nanny.hasMany(models.Trip, { onDelete: 'SET NULL', onUpdate: 'CASCADE' });
  
  sequelize.models.Nannies = Nanny;
  //sequelize.models.Nannies.belongsTo(sequelize.models.Schools);

  return Nanny;
};
