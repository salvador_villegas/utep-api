"use strict";

var fs        = require("fs");
var path      = require("path");
var Sequelize = require("sequelize");
var env       = process.env.NODE_ENV || "development";
var config    = require(__dirname + '/../config/config.json')[env];

if(env == 'production'){
  var match = process.env.DATABASE_URL.match(/postgres:\/\/([^:]+):([^@]+)@([^:]+):(\d+)\/(.+)/)
  console.log(match);
  sequelize = new Sequelize(match[5], match[1], match[2], {
      dialect:  'postgres',
      protocol: 'postgres',
      port:     match[4],
      host:     match[3],
      logging: false
  });
} else {
  var sequelize = new Sequelize(config.database, config.username, config.password, config);
}

var db        = {};

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  .forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });


  sequelize.models.Nannies.sync({force: true}).then(function () {
    // Table created
    return sequelize.models.Nannies.create({
      id: '1',
      username: 'Nanny 01',
      name: 'Juana Pérez',
      SchoolId: 1
    });
  });

  sequelize.models.Schools.sync({force: true}).then(function () {
    // Table created
    return sequelize.models.Schools.create({
      id: '1',
      name: 'Normal Isidro Burgos'
    });
  });

  sequelize.models.Trips.sync({force: true});

  sequelize.models.Nannies.belongsTo(sequelize.models.Schools);
  sequelize.models.Nannies.hasMany(sequelize.models.Trips);
  sequelize.models.Trips.belongsTo(sequelize.models.Nannies);
  sequelize.models.Schools.hasMany(sequelize.models.Nannies);


Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;