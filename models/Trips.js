"use strict";

module.exports = function(sequelize, DataTypes) {
  var Trip = sequelize.define("Trips", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    title: DataTypes.STRING,
  }/*, {
    classMethods: {
      associate: function(models) {
        Trip.belongsTo(models.Nannies, {
          onDelete: "CASCADE",
          foreignKey: 'id'
        });

      }
    }
  }*/);

  sequelize.models.Trips = Trip;
//  sequelize.models.Trips.belongsTo(sequelize.models.Nannies);


  return Trip;
};