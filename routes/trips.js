var models  = require('../models');
var express = require('express');
var router = express.Router();

/* GET /trips listing. */
router.get('/', function(req, res, next) {
  models.Trip.findAll({
    include: [ models.Nanny ]
  }).then(function(trips) {
    res.json(trips);
  });
});

router.post('/', function(req, res, next) {
  models.Trip.create(req.body)
  .then(function(trip) {
    console.log(trip.get({
      plain: true
    }));
    res.json(trip);
  });
});

/* GET /nannies/id */
router.get('/:id', function(req, res, next) {
  models.Nanny.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  }).populate('_school');
});

/* PUT /nannies/:id */
router.put('/:id', function(req, res, next) {
  models.Nanny.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE /nannies/:id */
router.delete('/:id', function(req, res, next) {
  models.Nanny.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
