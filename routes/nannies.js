var models  = require('../models');
var express = require('express');
var router = express.Router();

var Sequelize = require("sequelize");
var env       = process.env.NODE_ENV || "development";
var config    = require(__dirname + '/../config/config.json')[env];
var sequelize = new Sequelize(config.database, config.username, config.password, config);


/* GET /nannies listing. */
router.get('/', function(req, res, next) {
  models.Nannies.findAll({
    //include: [ models.School ]
  }).then(function(nannies) {
    res.json(nannies);
  });
});

router.post('/', function(req, res, next) {
  models.Nanny.create(req.body)
  .then(function(nanny) {
    console.log(nanny.get({
      plain: true
    }));
    res.json(nanny);
  });
});

/* GET /nannies/id */
router.get('/:id', function(req, res, next) {
  models.Nanny.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  models.Nanny.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
  models.Nanny.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
