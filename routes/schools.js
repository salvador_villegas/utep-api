var models  = require('../models');
var express = require('express');
var router = express.Router();

/* GET /nannies listing. */
router.get('/', function(req, res, next) {
  models.Trip.findAll({
    include: [ models.Nanny ]
  }).then(function(trips) {
    res.json(trips);
  });
});

router.post('/', function(req, res, next) {
  models.Trip.create(req.body)
  .then(function(trip) {
    console.log(trip.get({
      plain: true
    }));
    res.json(trip);
  });
});

/* GET /nannies/id */
router.get('/:id', function(req, res, next) {
  School.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  School.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
  School.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
